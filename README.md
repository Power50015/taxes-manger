# Taxes Manger APP

![Preview Img](screencapture.png)

Preview [Preview](https://taxes-manger.netlify.app/)

## Quickstart

1. Install the [node.js](https://nodejs.org/en/)
2. Clone the project

    ```bash
    git clone https://Power50015@bitbucket.org/Power50015/taxes-manger.git
    ```
3. Create firebase database (firestore & firebase Authentication)
4. Create firebase APP 
5. Go to project folder and add firebase app data to .env.example Template file and remove .example from the file name
3. Go run

    ```bash
    npm install
    ```

4. Start development mode

    ```bash
    npm run serve
    ```

5. In browser open page with address [http://localhost:8000/](http://localhost:8000/)

### Main tasks

- npm run serve -  launches watchers and server & compile project.
- npm run build - optimize & minify files for production version.

## INTRODUCTION & FEATURE

Creative & Modern site is a perfect template for Business Startups, web
studio and creative agencies. This is multi page for placing your
information. All files and code has been well organized and nicely commented for easy to customize.


## MAIN FEATURES :

- Valid HTML5, CSS3.
- Single Page APP.
- Vue 3 Composition.
- Fully Customizable.
- Clean Code.
- Fully Responsive.


## FILES INCLUDED :

- Vue Files.
- JS Files.

## Credits

- Vue 3
- VueX 4
- Vue CLI
- Vue Router
- Bootstrab 5
- Google Fonts 

## Support:

- If you need any help using the file or need special customizing please contact me via my Github or my Website.
- If you like my html template, please follwo me , We’ll appreciate it very much Thank you.
