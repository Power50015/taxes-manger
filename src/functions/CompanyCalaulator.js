export default function CompanyCalaulator(totalSlide, companyType) {
    let operations = [];
    let totalClear = 0;
    let totalTax = 0;
    if (companyType == "1") {
        totalTax = totalSlide * 0.20;
        totalClear = totalSlide - totalTax;
        operations.push({
            slide: totalSlide,
            percentage: 20,
            tax: totalTax,
            clear: totalClear,
        });
    } else if (companyType == "2") {
        totalTax = totalSlide * 0.19;
        totalClear = totalSlide - totalTax;
        operations.push({
            slide: totalSlide,
            percentage: 19,
            tax: totalTax,
            clear: totalClear,
        });
    }
    else if (companyType == "3") {
        totalTax = totalSlide * 0.23;
        totalClear = totalSlide - totalTax;
        operations.push({
            slide: totalSlide,
            percentage: 23,
            tax: totalTax,
            clear: totalClear,
        });
    }
    else if (companyType == "4") {
        totalTax = totalSlide * 0.26;
        totalClear = totalSlide - totalTax;
        operations.push({
            slide: totalSlide,
            percentage: 26,
            tax: totalTax,
            clear: totalClear,
        });
    }

    return {
        totalSlide: totalSlide,
        totalTax: totalTax,
        totalClear: totalClear,
        operations: operations
    }
}