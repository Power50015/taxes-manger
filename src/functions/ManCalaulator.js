export default function ManCalaulator(totalSlide) {
    let operations = [];
    let totalTax = 0;
    let totalClear = 0;
    if (totalSlide > 1000009) {
        let income = totalSlide;
        operations.push({
            slide: 400000,
            percentage: 22.5,
            tax: 90000,
            clear: 310000,
        });
        income = income - 400000;
        let incomeTax = income * 0.25;
        let clearIncome = income - incomeTax;
        operations.push({
            slide: income,
            percentage: 25,
            tax: incomeTax,
            clear: clearIncome,
        });
        totalTax = 90000 + incomeTax;
        totalClear = 310000 + clearIncome;

    } else if (totalSlide > 900009) {
        let income = totalSlide;
        operations.push(
            {
                slide: 200000,
                percentage: 20,
                tax: 40000,
                clear: 160000,
            },
            {
                slide: 200000,
                percentage: 22.5,
                tax: 45000,
                clear: 155000,
            }
        );
        income = income - 400000;
        let incomeTax = income * 0.25;
        let clearIncome = income - incomeTax;
        operations.push({
            slide: income,
            percentage: 25,
            tax: incomeTax,
            clear: clearIncome,
        });
        totalTax = 40000 + 45000 + incomeTax;
        totalClear = 160000 + 155000 + clearIncome;
    } else if (totalSlide > 800009) {
        var income = totalSlide;
        operations.push(
            {
                slide: 60000,
                percentage: 15,
                tax: 9000,
                clear: 51000,
            },
            {
                slide: 140000,
                percentage: 20,
                tax: 28000,
                clear: 112000,
            },
            {
                slide: 200000,
                percentage: 22.5,
                tax: 45000,
                clear: 155000,
            }
        );
        income = income - (60000 + 140000 + 200000);
        let incomeTax = income * 0.25;
        let clearIncome = income - incomeTax;
        operations.push({
            slide: income,
            percentage: 25,
            tax: incomeTax,
            clear: clearIncome,
        });
        totalTax = 9000 + 28000 + 45000 + incomeTax;
        totalClear = 51000 + 112000 + 155000 + clearIncome;
    } else if (totalSlide > 700009) {
        let income = totalSlide;
        operations.push(
            {
                slide: 45000,
                percentage: 10,
                tax: 4500,
                clear: 40500,
            },
            {
                slide: 15000,
                percentage: 15,
                tax: 2250,
                clear: 12750,
            },
            {
                slide: 140000,
                percentage: 20,
                tax: 28000,
                clear: 112000,
            },
            {
                slide: 200000,
                percentage: 22.5,
                tax: 45000,
                clear: 155000,
            }
        );
        income = income - (45000 + 15000 + 140000 + 200000);
        let incomeTax = income * 0.25;
        let clearIncome = income - incomeTax;
        operations.push({
            slide: income,
            percentage: 25,
            tax: incomeTax,
            clear: clearIncome,
        });
        totalTax = 4500 + 2250 + 28000 + 45000 + incomeTax;
        totalClear = 40500 + 12750 + 112000 + 155000 + clearIncome;
    } else if (totalSlide > 600009) {
        let income = totalSlide;
        operations.push(
            {
                slide: 30000,
                percentage: 2.5,
                tax: 750,
                clear: 29250,
            },
            {
                slide: 15000,
                percentage: 10,
                tax: 1500,
                clear: 13500,
            },
            {
                slide: 15000,
                percentage: 15,
                tax: 2250,
                clear: 12750,
            },
            {
                slide: 140000,
                percentage: 20,
                tax: 28000,
                clear: 112000,
            },
            {
                slide: 200000,
                percentage: 22.5,
                tax: 45000,
                clear: 155000,
            }
        );
        income = income - (30000 + 15000 + 15000 + 140000 + 200000);
        let incomeTax = income * 0.25;
        let clearIncome = income - incomeTax;
        operations.push({
            slide: income,
            percentage: 25,
            tax: incomeTax,
            clear: clearIncome,
        });
        totalTax = 750 + 1500 + 2250 + 28000 + 45000 + incomeTax;
        totalClear =
            29250 + 13500 + 12750 + 112000 + 155000 + clearIncome;
    } else {
        let income = totalSlide;
        if (income > 400009) {
            operations.push(
                {
                    slide: 15000,
                    percentage: 0,
                    tax: 0,
                    clear: 15000,
                },
                {
                    slide: 15000,
                    percentage: 2.5,
                    tax: 375,
                    clear: 14625,
                },
                {
                    slide: 15000,
                    percentage: 10,
                    tax: 1500,
                    clear: 13500,
                },
                {
                    slide: 15000,
                    percentage: 15,
                    tax: 2250,
                    clear: 12750,
                },
                {
                    slide: 140000,
                    percentage: 20,
                    tax: 28000,
                    clear: 112000,
                },
                {
                    slide: 200000,
                    percentage: 22.5,
                    tax: 45000,
                    clear: 155000,
                }
            );
            income = income - (15000 + 15000 + 15000 + 15000 + 140000 + 200000);
            let incomeTax = income * 0.25;
            let clearIncome = income - incomeTax;
            operations.push({
                slide: income,
                percentage: 25,
                tax: incomeTax,
                clear: clearIncome,
            });
            totalTax = 0 + 375 + 1500 + 2250 + 28000 + 45000 + incomeTax;
            totalClear =
                15000 + 14625 + 13500 + 12750 + 112000 + 155000 + clearIncome;
        } else if (income > 200009) {
            operations.push(
                {
                    slide: 15000,
                    percentage: 0,
                    tax: 0,
                    clear: 15000,
                },
                {
                    slide: 15000,
                    percentage: 2.5,
                    tax: 375,
                    clear: 14625,
                },
                {
                    slide: 15000,
                    percentage: 10,
                    tax: 1500,
                    clear: 13500,
                },
                {
                    slide: 15000,
                    percentage: 15,
                    tax: 2250,
                    clear: 12750,
                },
                {
                    slide: 140000,
                    percentage: 20,
                    tax: 28000,
                    clear: 112000,
                }
            );
            income = income - (15000 + 15000 + 15000 + 15000 + 140000);
            let incomeTax = income * 0.225;
            totalTax = 0 + 375 + 1500 + 2250 + 28000 + incomeTax;
            let clearIncome = income - incomeTax;
            operations.push({
                slide: income,
                percentage: 22.5,
                tax: incomeTax,
                clear: clearIncome,
            });
            totalClear =
                15000 + 14625 + 13500 + 12750 + 112000 + clearIncome;
        } else if (income > 60009) {
            operations.push(
                {
                    slide: 15000,
                    percentage: 0,
                    tax: 0,
                    clear: 15000,
                },
                {
                    slide: 15000,
                    percentage: 2.5,
                    tax: 375,
                    clear: 14625,
                },
                {
                    slide: 15000,
                    percentage: 10,
                    tax: 1500,
                    clear: 13500,
                },
                {
                    slide: 15000,
                    percentage: 15,
                    tax: 2250,
                    clear: 12750,
                }
            );
            income = income - (15000 + 15000 + 15000 + 15000);
            let incomeTax = income * 0.2;
            let clearIncome = income - incomeTax;
            operations.push({
                slide: income,
                percentage: 20,
                tax: incomeTax,
                clear: clearIncome,
            });
            totalTax = 0 + 375 + 1500 + 2250 + incomeTax;
            totalClear = 15000 + 14625 + 13500 + 12750 + clearIncome;
        } else if (income > 45009) {
            operations.push(
                {
                    slide: 15000,
                    percentage: 0,
                    tax: 0,
                    clear: 15000,
                },
                {
                    slide: 15000,
                    percentage: 2.5,
                    tax: 375,
                    clear: 14625,
                },
                {
                    slide: 15000,
                    percentage: 10,
                    tax: 1500,
                    clear: 13500,
                }
            );
            income = income - (15000 + 15000 + 15000);
            let incomeTax = income * 0.15;
            let clearIncome = income - incomeTax;
            operations.push({
                slide: income,
                percentage: 15,
                tax: incomeTax,
                clear: clearIncome,
            });
            totalTax = 0 + 375 + 1500 + incomeTax;
            totalClear = 15000 + 14625 + 13500 + clearIncome;
        } else if (income > 30009) {
            operations.push(
                {
                    slide: 15000,
                    percentage: 0,
                    tax: 0,
                    clear: 15000,
                },
                {
                    slide: 15000,
                    percentage: 2.5,
                    tax: 375,
                    clear: 14625,
                }
            );
            income = income - (15000 + 15000);
            let incomeTax = income * 0.1;
            let clearIncome = income - incomeTax;
            operations.push({
                slide: income,
                percentage: 10,
                tax: incomeTax,
                clear: clearIncome,
            });
            totalTax = 0 + 375 + incomeTax;
            totalClear = 15000 + 14625 + clearIncome;
        } else if (income > 15009) {
            operations.push({
                slide: 15000,
                percentage: 0,
                tax: 0,
                clear: 15000,
            });
            income = income - 15000;
            let incomeTax = income * 0.025;
            let clearIncome = income - incomeTax;
            operations.push({
                slide: income,
                percentage: 10,
                tax: incomeTax,
                clear: clearIncome,
            });
            totalTax = 0 + incomeTax;
            totalClear = 15000 + clearIncome;
        } else {
            let incomeTax = 0;
            let clearIncome = income - incomeTax;
            operations.push({
                slide: income,
                percentage: 0,
                tax: incomeTax,
                clear: clearIncome,
            });
            totalTax = incomeTax;
            totalClear = clearIncome;
        }
    }
    return {
        totalSlide: totalSlide,
        totalTax: totalTax,
        totalClear: totalClear,
        operations: operations
    }
}