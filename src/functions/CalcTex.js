import ManCalaulator from "@/functions/ManCalaulator";
import CompanyCalaulator from "@/functions/CompanyCalaulator";

export default function CalcTex(totalSlide, calcType, companyType) {
    if (calcType == "company") {
        return CompanyCalaulator(totalSlide, companyType)
    } else {
        return ManCalaulator(totalSlide);
    }
}