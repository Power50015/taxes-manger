import { createRouter, createWebHistory } from "vue-router";
import store from "@/store/index";
import Home from "@/views/Home.vue";
import Contact from "@/views/Contact.vue";
import Login from "@/views/Login.vue";
import Register from "@/views/Register.vue";
import Calculator from "@/views/Calculator.vue";
import Profile from "@/views/Profile.vue";
import Message from "@/views/Message.vue";
import Calculators from "@/views/Calculators.vue";
import SingleCalculator from "@/views/SingleCalculator.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: { middleware: "guest" }
  },
  {
    path: "/contact",
    name: "Contact",
    component: Contact,
    meta: { middleware: "guest" }
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
    meta: { middleware: "guest" }
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
    meta: { middleware: "guest" }
  },
  {
    path: "/calculator",
    name: "Calculator",
    component: Calculator,
    meta: { middleware: "user" }
  },
  {
    path: "/profile",
    name: "Profile",
    component: Profile,
    meta: { middleware: "user" }
  },
  {
    path: "/message",
    name: "Message",
    component: Message,
    meta: { middleware: "user" }
  },
  {
    path: "/calculators",
    name: "Calculators",
    component: Calculators,
    meta: { middleware: "user" }
  },
  {
    path: "/singleCalculator/:timeStamp",
    name: "SingleCalculator",
    component: SingleCalculator,
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, form, next) => {
  if (to.meta.middleware) {
    const middleware = require(`./middleware/index`);
    if (middleware) {
      middleware.default(next, store, to.meta.middleware);
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
