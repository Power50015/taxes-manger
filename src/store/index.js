import { createStore } from "vuex";

export default createStore({
  state() {
    return {
      isLogin: false,
      Id: "",
      Name: "",
      Email: "",
      TaxesId: "",
      CompanyAddress: ""
    };
  },
  mutations: {
    authLogin(state, payload) {
      state.isLogin = true;
      state.Id = payload.Id;
      state.Name = payload.name;
      state.Email = payload.email;
      state.TaxesId = payload.taxesId;
      state.CompanyAddress = payload.companyAddress;
    },
    authLogout(state) {
      state.isLogin = false;
      state.Id = "",
        state.Name = "";
      state.Email = "";
      state.TaxesId = "";
      state.CompanyAddress = "";
    }
  },
  actions: {},
  modules: {},
});
